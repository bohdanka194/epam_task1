﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EPAM_.NET_PracticeTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create vector1...");   
            Vector vector1 = new Vector(new int[] { 30,40,50,60,70,80});
            Console.WriteLine("vector1 = " + vector1.ToString());         
            Thread.Sleep(2000);

            Console.WriteLine("\nCreate vector2...");     
            Vector vector2 = new Vector(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 2, 7);
            Console.WriteLine("vector2 = " + vector2.ToString());
            Thread.Sleep(2000);
            Console.WriteLine("\nvector1 = vector1 + vector2");            
            vector1.AddVector(vector2);
            Console.WriteLine("vector1 = " + vector1.ToString());
            Thread.Sleep(4000);

            Console.WriteLine("\nCreate vector3...");
            Vector vector3 = new Vector(new int[] {5,5,5,5,5,5 });
            Console.WriteLine("vector3 = " + vector3.ToString());
            Thread.Sleep(4000);
            Console.WriteLine("\nvector1 = vector1 - vector3");
            vector1.SubtractVector(vector3);            
            Console.WriteLine("vector1 = " + vector1.ToString());
            Thread.Sleep(4000);

            double coefficient = 5;
            Console.WriteLine("\nvector3 = vector3 * " + coefficient);
            vector3.MultiplyByCoefficient(coefficient);
            Console.WriteLine("vector3 = " + vector3.ToString());
            Thread.Sleep(4000);
            Console.WriteLine("\nvector1 == vector2 ? " + vector1.CompareWithVectors(vector2));
            Thread.Sleep(3000);
            Console.WriteLine("\nElement in vector1[index=3] = " + vector1[3]);
            Thread.Sleep(2000);
            Console.WriteLine("\nSuccessfully completed the program!!!");
           
            Console.Read();
        }
    }
}
